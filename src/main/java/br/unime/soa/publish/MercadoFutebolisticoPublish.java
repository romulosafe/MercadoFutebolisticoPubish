package br.unime.soa.publish;

import org.apache.juddi.api_v3.AccessPointType;
import org.apache.juddi.v3.client.config.UDDIClerk;
import org.apache.juddi.v3.client.config.UDDIClient;
import org.uddi.api_v3.AccessPoint;
import org.uddi.api_v3.BindingTemplate;
import org.uddi.api_v3.BindingTemplates;
import org.uddi.api_v3.BusinessEntity;
import org.uddi.api_v3.BusinessService;
import org.uddi.api_v3.Name;

/** Servi�o de publica��o do Soap Web Service do Mercado Futebolistico 
 * @author R�mulo Ferreira
 * @since 19/10/2016
 */
public class MercadoFutebolisticoPublish {

	private static UDDIClerk clerk = null;
	
	/**  
	 */
	public MercadoFutebolisticoPublish() {

		try {

			UDDIClient uddiClient = new UDDIClient("META-INF/uddi.xml");
			clerk = uddiClient.getClerk("default");

			if (clerk==null)
				throw new Exception("Arquivo de configura��o incorreto!");

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	public void publish() {
		try {
			BusinessEntity MercadoFutebolisticoBusiness = new BusinessEntity();
			Name nomeDoBusiness = new Name();
			nomeDoBusiness.setValue("MercadoFutebolistico");
			MercadoFutebolisticoBusiness.getName().add(nomeDoBusiness);

			// Publicando o Business
			BusinessEntity register = clerk.register(MercadoFutebolisticoBusiness);
			if (register == null) {
				System.out.println("Save failed!");
				System.exit(1);
			}

			String myBusKey = register.getBusinessKey();

			// Criando o servi�o, e publicando no Business
			BusinessService MercadoFutebolisticoService = new BusinessService();
			MercadoFutebolisticoService.setBusinessKey(myBusKey);
			Name nomeDoServico = new Name();
			nomeDoServico.setValue("MercadoFutebolistico");
			MercadoFutebolisticoService.getName().add(nomeDoServico);

			// Criando o biding Template
			BindingTemplate mercadoBidingTemplate = new BindingTemplate();
			AccessPoint accessPoint = new AccessPoint();
			accessPoint.setUseType(AccessPointType.WSDL_DEPLOYMENT.toString());
			accessPoint.setValue("localhost:8081/MercadoFutebolistico/services/Mercado?wsdl");
			mercadoBidingTemplate.setAccessPoint(accessPoint);
			BindingTemplates mercadoBindingTemplates = new BindingTemplates();

			//optional but recommended step, this annotations our binding with all the standard SOAP tModel instance infos
			mercadoBidingTemplate = UDDIClient.addSOAPtModels(mercadoBidingTemplate);
			mercadoBindingTemplates.getBindingTemplate().add(mercadoBidingTemplate);
			MercadoFutebolisticoService.setBindingTemplates(mercadoBindingTemplates);

			// Salvando estrutura e publicando tudo
			BusinessService svc = clerk.register(MercadoFutebolisticoService);

			if (svc==null){
				System.out.println("Save failed!");
				System.exit(1);
			}

			clerk.discardAuthToken();

			System.out.println("Success!");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static void main(String[] args) {
		MercadoFutebolisticoPublish sp = new MercadoFutebolisticoPublish();
		sp.publish();
	}

}
